
<div class="row">
    <div class="col-md-9">
        <?php
            $title = '<h2 class="category-php">';
            if ($search_category != null) {
                $title .= 'Category: ' . strtoupper($search_category);
            }

            if (($search_category != null) && ($search_tag != null)) {
                $title .= ', ';
            }

            if ($search_tag != null) {
                $title .= "Tagged as '" . $search_tag . "'";
            }
            $title .= '</h2>';
            echo $title;
        ?>
        <p style="text-justify:distribute">php</p>


    </div>

    <div class="col-md-3">
        <h3>Popular tags</h3>
        <span class="label label-primary category-php-label">Security</span>
        <span class="label label-primary category-php-label">Tools</span>
        <span class="label label-primary category-php-label">Techniques</span>
    </div>

</div>

<hr>
