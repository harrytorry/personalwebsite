@extends('app')


@section('content')
    <?php
        $url = URL::previous();
        $exploded = explode('/', $url);
        $category = null;
        $tag = null;
        if (isset($exploded[5])) {
            $category = $exploded[5];
        }
        if (isset($exploded[7])) {
            $tag = $exploded[7];
        }

    ?>
    <ol class="breadcrumb">
        <li><a href="/blog">Blog</a></li>
        <?php if ($category != null) {
            echo '<li class=""><a href="/blog/category/' . $category . '">' . $category . '</a></li>';
        }
        ?>

        <?php if ($tag != null) {
            echo '<li class=""><a href="/blog/category/' . $category . '/tag/' . $tag . '">' . $tag . '</a></li>';
        }
        ?>
        <li class="active">{{ isset($post->title) ? $post->title : 'Default' }}</li>
    </ol>

    <h1>{{ $post->title }}</h1>
    {!! Markdown::parse($post->body) !!}

    <div id="disqus_thread"></div>
    <script type="text/javascript">
        /* * * CONFIGURATION VARIABLES * * */
        var disqus_shortname = 'harrytorry';

        /* * * DON'T EDIT BELOW THIS LINE * * */
        (function() {
            var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
            dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
        })();
    </script>
    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>

@endsection
