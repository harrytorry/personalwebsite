@extends('app')

@section('content')

    @include('blog.categoryHeader.' . $search_category)

    @if (count($posts) > 0)
        @foreach($posts as $post)
            <h2><a style="color:#444" href="/blog/{{ $post->id }}/{{ $post->seoTitle }}">{{ $post->title }}</a></h2>
            <p class="text-justify">{{ $post->preview }}...</p>
            <p>
                <span class="label label-default">{{ $post->prettyDate }}</span>
                @foreach($post->tagsArray as $tag)
                    <a href="/blog/category/{{$post->category}}/tag/{{$tag}}"><span class="label label-primary category-{{$post->category}}-label">{{ $tag }}</span></a>
                @endforeach
            </p>
        @endforeach
        {!! $posts->render() !!}

    @else
        <h2>No results found!</h2>
    @endif
@endsection
