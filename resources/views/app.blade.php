<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Harry Torry</title>

	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/blog">Harry Torry</a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li><a href="{{ url('/projects') }}">Projects</a></li>
				</ul>

				<ul class="nav navbar-nav navbar-right">
					@if (Auth::guest())
						<li><a href="{{ url('/auth/login') }}">Login</a></li>
						<li><a href="{{ url('/auth/register') }}">Register</a></li>
					@else
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="{{ url('/auth/logout') }}">Logout</a></li>
							</ul>
						</li>
					@endif
				</ul>
			</div>
		</div>
	</nav>

	<div class="container-fluid">
		<div class="row">
			<div class="col-md-9 content-body">
				<div class="row">
					<div class="col-md-2 left-sidebar">

						<?php
							$sidebar = array();
							$sidebar[] = array(
									'name' => 'PHP',
									'category' => 'php',
									'children' => array(
											['name' => 'Laravel', 'tag' => 'laravel'],
											['name' => 'Libraries', 'tag' => 'libraries'],
											['name' => 'News', 'tag' => 'news'],
									)
							);
							$sidebar[] = array(
									'name' => 'Node.js',
									'category' => 'nodejs',
									'children' => array(
											['name' => 'Security', 'tag' => 'security'],
											['name' => 'Modules', 'tag' => 'modules'],
									)
							);
							$sidebar[] = array(
									'name' => 'Hosting',
									'category' => 'hosting',
									'children' => array(
											['name' => 'Jelastic', 'tag' => 'jelastic'],
											['name' => 'Linode', 'tag' => 'linode'],
											['name' => 'Chunkhost', 'tag' => 'chunkhost'],
									)
							);
							$sidebar[] = array(
									'name' => 'Security',
									'category' => 'security',
									'children' => array(
											['name' => 'SSL', 'tag' => 'ssl'],
											['name' => 'Server Protection', 'tag' => 'server protection'],
									)
							);
						?>

						@foreach ($sidebar as $item)
							<div class="sidebar sidebar-{{$item['category']}}">
								<a href="/blog/category/{{ $item['category'] }}"><h2>{{ $item['name'] }}</h2></a>
								<ul>
									@foreach ($item['children'] as $tag)
									<li><a href="/blog/category/{{ $item['category'] }}/tag/{{ $tag['tag'] }}">{{ $tag['name'] }}</a></li>
									@endforeach
								</ul>
							</div>
						@endforeach

					</div>
					<div class="col-md-10">
						@yield('content')
					</div>
				</div>
			</div>

			<div class="col-md-3 right-sidebar">
				<br/>
				<div class="input-group" id="site-search">
					<input type="text" class="form-control" placeholder="e.g. PHP Security">
					  <span class="input-group-btn">
						<button class="btn btn-default" type="button">Go!</button>
					  </span>
				</div>
				<br/>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Popular Posts</h3>
					</div>
					<div class="panel-body">
						<ul>
							<li><a href="#">article 1</a></li>
							<li><a href="#">article 2</a></li>
							<li><a href="#">article 3</a></li>
							<li><a href="#">article 4</a></li>
							<li><a href="#">article 5</a></li>
						</ul>
					</div>
				</div>

				<div class="input-group">
					<div class="input-group-btn">
						<!-- Single button -->
						<div class="btn-group popup">
							<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
								Subscribe <span class="caret"></span>
							</button>
							<ul class="dropdown-menu" role="menu">
								<li><a href="#">This is my email address</a></li>
								<li><a href="#">This is my webhook URL</a></li>
							</ul>
						</div>
					</div>
					<input type="text" class="form-control" aria-label="..." placeholder="Want updates?">
				</div>
				<br/>

				<div class="well well-sm">
					Harry Torry, Web developer, Cheltenham.
				</div>

				<div class="well well-sm">
					<p>Adverts?</p>
				</div>
			</div>
		</div>
	</div>

	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
</body>
</html>
