@extends('app')

@section('content')

    <!-- Nav tabs -->
    <ul class="nav nav-tabs">
        <li class="active">
            <a href="#tab0" data-toggle="tab" aria-expanded="true">Project 1</a>
        </li>
        <li>
            <a href="#tab1" data-toggle="tab">Project 2</a>
        </li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane active" id="tab0">
            <h2>An awesome project</h2>
        </div>
        <div class="tab-pane" id="tab1">
            <h2>Something even cooler</h2>
        </div>
    </div>
@endsection
