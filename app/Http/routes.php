<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Front facing
	Route::get('/', function() {
		redirect('/blog');
	});

	// blog
	Route::get('blog', 'BlogPostController@index');
	Route::get('blog/search/{criteria}', 'BlogPostController@search');
	Route::get('blog/category/{category}/tag/{tag}', 'BlogPostController@showTags');
	Route::get('blog/category/{category}', 'BlogPostController@showCategories');
	Route::get('blog/{id}/{seoUrl}', 'BlogPostController@showPost');

	// Projects
	Route::get('projects', 'ProjectsController@index');

	// Faker
	Route::get('faker/blogs', function() {
		$faker = new Faker\Factory();

		for ($i = 0; $i < 100; $i++) {
			$faked = $faker->create();
			$faked->addProvider(Faker\Provider\Lorem::paragraphs(30));

			$tags = array("laravel", "callbacks", "open source", "phpunit", "symfony", "asd", "qwer");
			$rand_tags = array_rand($tags, 2);

			$categories = array('php', 'nodejs', 'hosting', 'security');
			$rand_category = array_rand($categories, 1);
			$titleLength = rand(20, 40);

			$blogPost = new App\BlogPost;
			$blogPost->author = 1;
			$blogPost->title = substr($faked->paragraph(), 0, $titleLength);
			$blogPost->body = implode('<br/>', $faked->paragraphs());
			$blogPost->preview = $faked->paragraph();
			$blogPost->tags = $tags[$rand_tags[0]] . "," . $tags[$rand_tags[1]];
			$blogPost->category = $categories[$rand_category];
			$blogPost->publishedOn = $faked->dateTime;

			$blogPost->save();
		}
		dd('Should have created 100 entries');

	});


// Backend
Route::get('admin/blog/create', 'BlogPostController@create');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
