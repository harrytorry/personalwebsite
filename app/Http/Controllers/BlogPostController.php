<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\BlogPost;
use Carbon\Carbon;

use Illuminate\Http\Request;

class BlogPostController extends Controller {

	protected $paginationNumber = 6;

	public function prettyDate($date)
	{
		return Carbon::parse($date)->format('d / M / Y');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @param null $posts
	 * @param string $category
	 * @param null $tags
	 * @param null $criteria
	 * @return Response
	 */
	public function index(
		$posts = null,
		$category = 'default',
		$tags = null,
		$criteria = null
	)
	{
		if ($posts == null) {
			$posts = BlogPost::where('author', '=', '1')
				->orderBy('publishedOn', 'desc')
				->paginate($this->paginationNumber);
		}
		$pages = $posts->count();
		foreach($posts as $post) {
			$post->bodySnippet = substr($post->body, 0, 300);
			$post->seoTitle = strtolower(str_replace(' ', '-', $post->title));
			$post->tagsArray = (array) array_map('trim', explode(',', $post->tags));
			$post->prettyDate = $this->prettyDate($post->publishedOn);
		}
		return view(
			'blog.index',
			[
				'posts' 			=> $posts,
				'search_category' 	=> $category,
				'search_tag' 		=> $tags,
				'search_text' 		=> $criteria
			]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('blog.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function showPost($id)
	{
		$post = BlogPost::find($id);
		return view('blog.show', ['post' => $post]);
	}

	public function showTags($category, $tag)
	{
		$posts = BlogPost::where('tags', 'LIKE', '%' . $tag . '%')
			->orderBy('publishedOn', 'desc')
			->paginate($this->paginationNumber);
		return $this->index($posts, $category, $tag);
	}

	public function showCategories($category)
	{
		$posts = BlogPost::where('category', 'LIKE', '%' . $category . '%')
			->orderBy('publishedOn', 'desc')
			->paginate($this->paginationNumber);
		return $this->index($posts, $category);
	}

	public function search($criteria)
	{
		$posts = BlogPost::where('body', 'LIKE', '%' . $criteria . '%')
		//	->where('title', 'LIKE', '%' . $criteria . '%')
		//	->where('body', 'LIKE', '%' . $criteria . '%')
			->orderBy('publishedOn', 'desc')
			->paginate($this->paginationNumber);
		return $this->index($posts, $category = 'search', null, $criteria);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
