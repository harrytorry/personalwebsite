<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogPost extends Model {

    protected $table = 'blog_posts';

    protected $fillable = ['author', 'title', 'body', 'tags', 'category', 'publishedOn', 'created_at', 'updated_at'];

    protected $gaurded = ['id'];
}
